# atelier_jupyter_r

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fgt-notebook%2Fworkshop%2Fworkshop_3_decembre_2021%2Fatelier_jupyter_r.git/main)

https://towardsdatascience.com/python-environment-101-1d68bda3094d

## Version pyenv + pyenv-virtualenv

## Version Pyenv + pipenv

### Clone du projet 

```
git clone git@gitlab.huma-num.fr:gt-notebook/workshop/workshop_3_decembre_2021/atelier_jupyter_r.git
```

### Installer R et IRKernel

Dans votre terminal
```
sudo apt-get install r-base
```

Dans R : 
```
install.packages('IRkernel') 
```

### Installation de Python 

Installer la version python 3 classique du gestionnaire de paquet.

### Installation de Pyenv

Permet de gérer différentes version de python sur la même machine. En gros cela créé des containers isolés avec des version de python et des packages différents.

https://github.com/pyenv/pyenv
https://github.com/pyenv/pyenv-installer


### Créer un environnement dédié pour Jupyter

- Installer une version de python, par exemple python 3.9.0

```
pyenv install 3.9.7
```

- Définir un python en local pour ce dossier 

```
pyenv local 3.9.7
python --version
```

### Installation de Pipenv

```
pip install --upgrade pip
pip install pipenv
```

### Créer un container pour le notebook 

```
pipenv install --python 3.9.7
pipenv shell
```

### Installer Jupyter lab et IPykernel

```
pipenv install jupyterlab
pipenv install ipykernel
python -m ipykernel install --user --name=`basename $VIRTUAL_ENV`
```

### Enregistrer le Kernel

Lancer R et installer le kernel spec (?)  **Attention il faut bien être dans le container pipenv pour que R voit le kernel jupyter que l'on a installé)

```
IRkernel::installspec(user=FALSE)
q()
```

### Lancer Jupyter Lab

```
jupyter lab
```

### Sortir du container 

Ctrl + D ou exit
